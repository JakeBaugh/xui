﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

	public float speed;
	ParticleSystem starField;

	private float starSpeedMulti = 75.0f;
	private float starMinSpeed = 25.0f;
	private float starMaxSpeed = 150.0f;

	private float x, y;

	void Start () {
		starField = GameObject.FindGameObjectWithTag("StarField").GetComponent<ParticleSystem>();
	}

	void Update () {
		x = Input.GetAxis("Horizontal") * Time.deltaTime * speed;
		y = Input.GetAxis("Vertical") * Time.deltaTime * speed;
		this.transform.Translate(x, y, 0, Space.World);

		if(this.transform.localPosition.x > 18.6f) {
			this.transform.localPosition = new Vector3(-7.2f, this.transform.localPosition.y, this.transform.localPosition.z);
		} else if (this.transform.localPosition.x < -7.1) {
			this.transform.localPosition = new Vector3(18.5f, this.transform.localPosition.y, this.transform.localPosition.z);
		}
	}

	void LateUpdate () {

		// Update particle velocities, assumes constant velocity
		int numParticles = starField.particleCount;
		ParticleSystem.Particle[] particles = new ParticleSystem.Particle[numParticles];
		starField.GetParticles(particles);
		
		for (int i = 0; i < particles.Length; i++)
		{
			var particle = particles[i];

			particle.velocity = new Vector3(particle.velocity.x,
			                                particle.velocity.y,
			                                Mathf.Clamp(particle.velocity.z + (y * starSpeedMulti), starMinSpeed, starMaxSpeed));

			//print (particle.velocity.z);

			particles[i] = particle;
		}
		
		starField.SetParticles(particles, numParticles);

	}

}
