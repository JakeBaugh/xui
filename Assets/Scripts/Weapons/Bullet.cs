﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	public float speed = 15.0f;

	void Update () {
		this.transform.Translate (Vector3.up * speed * Time.deltaTime);
		if(this.transform.position.y > 50){
			Destroy(this.gameObject);
		}
	}


}
