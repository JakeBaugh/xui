﻿using UnityEngine;
using System.Collections;

public class Gun : MonoBehaviour {

	enum fireMode
		{
			singleFire,
			rapidFire
		}

	protected Vector3 positionOffset;
	private Vector3 shootDirection;	
	public GameObject[] bullets;
	public float fireRate = 15.0f;

	private fireMode fMode = fireMode.singleFire;
	private float nextFireTime = 0;
	private Transform projectileBin;

	private bool isRapidFire = false;
	
	void Start () {
		projectileBin = GameObject.FindGameObjectWithTag ("BinProjectile").transform;
	}

	void Update () {
		fire (0);

		if(Input.GetKeyDown(KeyCode.Q)){
			isRapidFire = !isRapidFire;
		}

		switch (isRapidFire) {
			case false:
					fMode = fireMode.singleFire;
					break;
			case true:
					fMode = fireMode.rapidFire;
					break;
			}
	}

	public void fire (int bulletIndex) {
		switch (fMode) {

		case fireMode.singleFire:
			if (Input.GetButtonDown ("Fire")) {
				initProjectile(0);
				nextFireTime += 0.05f;
				this.GetComponent<AudioSource>().Play();
			}
			break;
			
		case fireMode.rapidFire:
			if (Input.GetButton ("Fire")) {
				if(Time.time - fireRate > nextFireTime){
					nextFireTime = Time.time - Time.deltaTime;
				}
				while (nextFireTime < Time.time) {
					initProjectile(0);
					nextFireTime += fireRate;
					this.GetComponent<AudioSource>().Play();
				}
			}
			break;
		}
	}

	private void initProjectile (int bulletIndex) {
		GameObject tmpBullet = Instantiate ((Object)bullets[bulletIndex], this.transform.position, Quaternion.identity) as GameObject; 
		tmpBullet.transform.parent = projectileBin;
	}
}
